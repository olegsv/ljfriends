/**
 * Saves into IndexedDB storage
 * @returns {undefined}
 */
function ljstorage() {
    this.DBNAME = "ljfriends";
    this.DBVERSION = 1;
    this._db = null;
    this._r = new Date();

};

ljstorage.prototype = {
    DBNAME : "ljfriends",
    DBVERSION : 1,
    _db : null,
    _r : 'mad oop test here'
};

ljstorage.prototype.getStats = function() {
    this.openDB( this.fetchStats );
};

ljstorage.prototype.setStats = function( stats ) {
    var req_open = indexedDB.open( this.DBNAME, this.DBVERSION );
};


ljstorage.prototype.openDB = function(called_func) {
    if( !("indexedDB" in window)) {
        throw "IndexedDB not supported";
    }

    var req_open = indexedDB.open( this.DBNAME, this.DBVERSION );

    req_open.onerror = this.onErrorFunc.bind(this);
    req_open.onsuccess = called_func.bind(this);
    req_open.onupgradeneeded = this.onUpgradeFunc.bind(this);
    req_open.onblocked = this.onBlockedFunc.bind(this);

    return req_open;

    
};


ljstorage.prototype.checkSchemaExists = function( func ) {
    console.log("Check schema exists");

    var req_open = indexedDB.open( this.DBNAME, this.DBVERSION );

    req_open.onerror = this.onErrorFunc.bind(this);
    req_open.onsuccess = this.onSuccessFunc.bind(this,func);
    req_open.onupgradeneeded = this.onUpgradeFunc.bind(this);
    req_open.onblocked = this.onBlockedFunc.bind(this);
};



ljstorage.prototype.onErrorFunc = function(e) {
    console.log("Error opening DB");
    console.dir( e);
};

ljstorage.prototype.onBlockedFunc = function(e) {
    throw "Error: database blocked";
};

/**
 * 
 * @param {Event} e
 * @returns {undefined}
 * The upgradeneeded event is used both when the user first opens the database as well as when extension version is changed
 */
ljstorage.prototype.onUpgradeFunc = function(e) {
    this._db = e.target.result;

    var v = localStorage.ljfriends_version || 0;

    console.log( "Upgrade: from version: "+v +" to version: "+this.DBVERSION );
    while( v < this.DBVERSION ) {
        switch( v ) {
            case 0:
                this.createObjects1( );
                break;
            // case 1:
            // add more here
        }
        v++;

    }
    localStorage.ljfriends_version = this.DBVERSION;
};


ljstorage.prototype.onSuccessFunc = function(callback_func, e) {
    console.log("Success!");
    this._db = e.target.result;
    callback_func();
}

/**
 * Create objects for version 1
 * @returns boolean
 */
ljstorage.prototype.createObjects1 = function() {

    if(!this._db.objectStoreNames.contains("history"))
        this._db.createObjectStore( "history", { "keyPath" : "day" } );
    if(!this._db.objectStoreNames.contains("friends"))
        this._db.createObjectStore( "friends", { "keyPath" : "user" } );
    if(!this._db.objectStoreNames.contains("stats"))
        this._db.createObjectStore( "stats", { "keyPath": "day" } );

    this._r = new Date();
    console.log("CreateObjects: Version "+this.DBVERSION+" created!");
};


ljstorage.prototype.madoop = function(e) {
    console.dir( e );
};
    