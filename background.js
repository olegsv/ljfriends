// Global variables only exist for the life of the page, so they get reset
// each time the page is unloaded.
// localStorage is persisted, so it's a good place to keep state that you
// need to persist across page reloads.
// var counter = 1;



chrome.runtime.onInstalled.addListener(function() {
  var storage = new ljstorage();
  // check schema exists, upon finishing start the loader.
  // the loader will load first time and will start the alarm
  storage.checkSchemaExists( function() { 
      console.log("Installed");
  });
  
});



/**
 * Alarm listener.
 * Runs periodic friends check and adds the alarm again
 */
chrome.alarms.onAlarm.addListener(function(alarm) {
    if( alarm.name === "checkljfriends" ) {
        ljloader.start();
    }
});

//chrome.browserAction.onClicked.addListener(function() {
//  // The event page will unload after handling this event (assuming nothing
//  // else is keeping it awake). The content script will become the main way to
//  // interact with us.
//  chrome.tabs.create({url: "http://google.com"}, function(tab) {
//    chrome.tabs.executeScript(tab.id, {file: "content.js"}, function() {
//      // Note: we also sent a message above, upon loading the event page,
//      // but the content script will not be loaded at that point, so we send
//      // another here.
//      sendMessage();
//    });
//  });
//});
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.todo == "load") {
      sendResponse({farewell: "goodbye"});
      //console.log("Received");
      ljloader.start();
    }
});

chrome.runtime.onStartup.addListener(function() {
    //console.log("Starting up.");
    ljloader.start();
    //chrome.browserAction.setBadgeText({text: "+++"});
});
chrome.runtime.onSuspend.addListener(function() {
    //console.log("Unloading.");
    //chrome.browserAction.setBadgeText({text: "---"});
});

chrome.runtime.onSuspendCanceled.addListener(function() {
    console.log("Suspend canceled.");
    //chrome.browserAction.setBadgeText({text: "..."});
});

// common listener


if( !localStorage["ljfriends_username"] ) {
    window.open( chrome.extension.getURL("options.html") );
} else {
    ljloader.start();
}







/*
function onAlarm(alarm) {
  // |alarm| can be undefined because onAlarm also gets called from
  // window.setTimeout on old chrome versions.
  if (alarm && alarm.name == 'watchdog') {
    onWatchdog();
  } else {
    startRequest({scheduleRequest:true, showLoadingAnimation:false});
  }
}
*/
/*
var loadingAnimation = new LoadingAnimation();

//var isOldVersion = !chrome.runtime; // Legacy support for pre-event-pages.
var requestTimerId;

chrome.runtime.onInstalled.addListener(onInit);
chrome.alarms.onAlarm.addListener(onAlarm);
chrome.browserAction.onClicked.addListener(goToInbox);

//if (chrome.runtime && chrome.runtime.onStartup) {
  chrome.runtime.onStartup.addListener(function() {
    console.log('Starting browser... updating icon.');
    startRequest({scheduleRequest:false, showLoadingAnimation:false});
    updateIcon();
});
*/