/*
 * ljfriends - show your lj friends statistics
 * 
 * Oleg Sverdlov <oleg@ols.co.il> 2013
 */

function ljuser(username, name, type ) {
    this._f = type;
    this.name = name;
    this.username = username;
    this.type = 'user';
    //this.status = status;
};

ljuser.prototype = {
    _f: 0,
    name: '',
    username: '',
    type: '',
    //status: null,
    FRIEND_MUTUAL : 3,
    FRIEND_MINE   : 1,
    FRIEND_OF     : 2,
    isMutual      : function() { return ( this._f == this.FRIEND_MUTUAL ); },
    getUsername   : function() { return this.username; },
    getFriend     : function() { return this._f; },
    getType       : function() { return this.type; },
    set : function(k,v) {
        switch( k) {
            case 'user': this.username = v; break;
            case 'name': this.name = v; break;
            case 'type': if( v==="user" || v==="community" ) { this.type = v; }; break;
            default: break; // fg, bg, groupmask
        }
    }
};



function ljstats( friends, friend_of, mutual ) {
    this._stats = [];
    this._stats["fof"] = friend_of;
    this._stats["friends"] = friends;
    this._stats["mutual"] = mutual;
    this._stats["friends_only"] = friends - mutual;
    this._stats["fof_only"] = friend_of - mutual;
    return this;
};


//ljstats.prototype.incr         = function(type) {  this._stats[type]++; }
//ljstats.prototype.decr         = function(type) {  this._stats[type]--; }
ljstats.prototype = {
    _stats : [],
    day : 'na',
    setDay    : function(s) { this.day = s; },
    loadFromJSON: function( tempstats ) { if( tempstats ) { this._stats = tempstats; }; return this; },
    getMutual : function() { return this._stats["mutual"]; },
    getFriends: function() { return this._stats["friends"]; },
    getFof    : function() { return this._stats["fof"]; },
    /**
     * Number of friends who never friended back
     * @returns {int}
     */
    getFriendsOnly: function() { return this._stats["friends_only"]; },
    /**
     * friendsOf whos requests I left unanswered
     * @returns {int}
     */
    getFofOnly: function() { return this._stats["fof_only"]; },

    setMutual : function(n) { this._stats["mutual"] = n; },
    setFriends: function(n) { this._stats["friends"] = n; this._stats['friends_only'] = n-this._stats['mutual']; },
    setFof    : function(n) { this._stats["fof"] = n;  this._stats['fof_only'] = n-this._stats['mutual'];}

    
};



function ljuserlist() {
    this.users = new Array();
    this.index = {};
    this.count = 0;
    this.min_index = 0;
    this.result = null; // value of "success" in LJ flat protocol
    this.message = null; // value of errmsg in lj flat.
};
ljuserlist.prototype = {
    users: [],
    index: {},
    count: 0,
    min_index: -1,
    result: 0, message: '',

    /**
     *
     * @returns {Number|@pro;length@this.users}
     */
    getCount: function() { return this.count || this.users.length; },
    /**
     * Add property to user. This is used while parsing getfriends or friendof requests.
     * Note that user id can not be 0
     * 
     * @param {int} n user number in userlist. >0. friend_100_type
     * @param {string} name parameter name
     * @param {string} val
     * @returns {undefined}
     */
    addProperty: function(n, name,val) {
        n = n-1;
        if( !this.users[n] ) {
            this.users[n] = new ljuser();
        }
        this.users[n].set( name, val );
        if( name === 'user' || name ==='username' ) {
            this.index[ val ] = n;
        };
    },
    setCount: function(n) { this.count=n; },
    /**
     * Push the user to the end of the list, increment counter.
     * If the user exists, override existing user.
     * @param {ljuser} user
     */
    put: function( user ) {
        if( this.index[ user.getUsername() ] ) {
            this.users[ this.index[ user.getUsername() ] ] = user;
        } else {
            this.users[ this.count ] = user;
            this.index[ user.getUsername() ] = this.count;
            this.count++;
        }
    },
    hasUsername: function(u) { 
        if( u instanceof ljuser ) {
            return ( undefined !== this.index[ u.getUsername() ] );
        } else {
            return ( undefined !== this.index[ u ] );
        }
    },
    getResult: function() { return this.result; },
    getMessage: function() { return this.msg; },
    setResult: function(str) { this.result = str; return this; },
    setMessage: function(str) { this.msg = str; return this; }
};





function ljcollection() {
    this.friends = null;
    this.fof = null;
    this.mutual = null;
    this._stats = null;
}
ljcollection.prototype = {
    friends: null,
    fof: null,
    mutual: null,
    _stats : null,
    /**
     *
     * @param {ljuserlist} userlist
     */
    setFriends : function ( userlist ) {
        this.friends = userlist;
    },
    /**
     *
     * @param {ljuserlist} userlist
     */
    setFof : function ( userlist ) {
        this.fof = userlist;
    },
    /**
     * Intersection between friends and fof lists
     */
    generateFriends: function() {
        this.mutual = new ljuserlist();
        // total friends
        var friends_len = this.friends.getCount();
        // total friends of
        var fof_len = this.fof.getCount();
        var fof_missing = 0;
        //
        var friends_missing = 0;
        // 
        var m = 0;
        if( friends_len > fof_len ) {
            for( var n=fof_len; n--; ) {
                if(  this.friends.hasUsername( this.fof.users[ n ].getUsername() ) )  {
                    this.mutual.put( this.fof.users[n] );
                } else {

                }
            }
        } else /* if( friends_len <= fof_len ) */ {
            for( var n=friends_len; n--; ) {
                if( this.fof.hasUsername( this.friends.users[ n ].getUsername() )  ) {
                    this.mutual.put( this.friends.users[n] );
                } else {
                }
            }
        }

        // stats
        this._stats = new ljstats( friends_len, fof_len, this.mutual.getCount() );
//        this._stats.setFofOnly( fof_len - this.mutual.getCount() );  // friendsOf whos requests I left unanswered
//        this._stats.setFriendsOnly( friends_len - this.mutual.getCount() ); // friends who never friended me back

    },
    /**
     * 
     * @returns {ljstats}
     */
    getStats : function() {
        return this._stats;
    }
}



function ljrequest( username) {
    this.username = username;
    this._result = null;
    this._cookies = [];
};

ljrequest.prototype = {
    URL : "http://www.livejournal.com/interface/flat",
    CHECK: 'checkfriends',
    GETFRIENDS : 'getfriends',
    FRIENDOF : 'friendof',
    _cookies : [],
    _result : null,
    username: null,
    getCookies: function() {
        chrome.cookies.getAll( { domain: "livejournal.com" }, this.coo );

    },
    coo: function( c ) {
        console.log( "received cookies");
        console.log( c );
    },

    /**
     *
     * @param t string 'checkfriends' || 'getfriends' || 'friendof'
     * @param func function to be called when request's onLoad event is fired. The function has 2 parameters:  t and the response.
     * @returns {undefined}
     */
    createRequest: function(t, func) {
        var xhr = new XMLHttpRequest();
        var fd = new FormData();
        fd.append( 'mode',t );
        fd.append( 'auth_method','cookie');
        fd.append( 'user', this.username );
        fd.append( 'ver', 1 );
        if( t === this.CHECK ) {
            fd.append( 'lastupdate','' );//2012-09-01 00:00:00');
            fd.append( 'mask', 0 );
        }
        xhr.responseType = "text";
        xhr.open("POST", this.URL, true);

        xhr.setRequestHeader("X-LJ-Auth", "cookie" );

        //xhr.onload = this.onLoadFunc.bind(this);
        xhr.onload = this.onLoadFunc.bind(this,t,func); // t is param2
        xhr.send(fd);
    },
    onLoadFunc: function(param2,callback_func,evt) {
        callback_func( param2, evt.target.responseText );
    }
};




/**
 *
 *
 */
var ljpopup = {
    refresh: function() {
        var u = localStorage["ljfriends_username"];

        if( !u ) {
            ljpopup.showSetup( "Set your username" );
            return;
        }

        if( !localStorage["ljfriends_error"] ) {
            console.log("No error");
            u = 'http://'+u+'.livejournal.com/friends';
            document.getElementById('stats_container').addEventListener('click', function() { window.open(u,'_newtab'); } );
            ljpopup.showPopup();/* ljloader.loadFriends(); */
        } else {
            ljpopup.showError( localStorage["ljfriends_error"] );
        }
    },
    /**
     * Show popup populated with current stats data
     * @returns null
     */
    showPopup: function() {
        document.getElementById('warning_container').setAttribute("class","div_hidden");
        document.getElementById('stats_container').removeAttribute("class");
        
        var storage = new ljstorage();
        storage.openDB( function(evt) {
            var db = evt.target.result;
            var transaction = db.transaction( ['stats'] , "readonly" ).objectStore("stats").get('current');
            transaction.onsuccess = function(e) {
                var res = e.target.result;
                if( (undefined!== res) && ("_stats" in res ) ) {
                    stats = new ljstats().loadFromJSON( res._stats );
                    document.getElementById('friends').innerText = stats.getFof();
                    document.getElementById('friends_mutual').innerText = stats.getMutual();
                    document.getElementById('friends_mine').innerText = stats.getFriendsOnly();
                    document.getElementById('friends_of').innerText = stats.getFofOnly();
                } else {
                    console.warn('Problem reading from DB:'+stats);
                }
            };
        });
    },
    /**
     * Show setup screen in case we have to set up username
     * @param {string} msg
     */
    showError: function(msg) {
        console.log('Error:'+localStorage['ljfriends_error'] );
        document.getElementById('stats_container').setAttribute("class","div_hidden");
        document.getElementById('message').innerText = msg;
        document.getElementById('reload').addEventListener('click', function() {
            ljloader.start();
        });
        document.getElementById('warning_container').removeAttribute("class");

    },
    showNotification: function( n ) {
        var msg;
        if( n>0 ) {
            msg = 'You have more friends...';
        } else if (n<0) {
            msg = 'You have less friends...';
        } else {
            msg = 'Some came, some left...'
        }
        var notification = webkitNotifications.createNotification(
            'icon.png',  // icon url - can be relative
            'Friends list changed',  // notification title
            msg // notification body text
        );
        notification.show();
        setTimeout( function(){
            notification.cancel();
        },2000);
    }
};




/**
 * Load two lists of friends from LJ flat API
 * Parse the lists
 * Count the stats
 * Store the stats in IndexedDB
 * @type type
 */
var ljloader = {
    fof: [],
    friends: [],
    finished_fof: false,
    finished_friends: false,
    result: 'waiting',
    timeout: 10,
    /**
     * Experimental XMLRPC - check if the user has new friends
     * @returns
     */
    checkFriendsUpdated: function() {
        if( !localStorage["ljfriends_username"] ) {
            console.log( "ljloader.start: username missing" );
            return;
        }

        var a = new XmlRpcRequest( "http://www.livejournal.com/interface/xmlrpc", "LJ.XMLRPC.checkfriends" );
        a.addParam( {
            username:  localStorage["ljfriends_username"],
            auth_method: 'cookie',
            ver: 1,
            lastupdate: '2013-10-20 00:00:00',
            mask: 0
        });
        a.setHeader( "X-LJ-Auth", "cookie" );

        resp = a.send();
        resp.parseXML();
        console.dir(resp.params);
    },

    /**
     * Load friends.
     * Set the next friend load event timer to +5 min
     *
     * @returns {unresolved}
     */
    start: function() {
        chrome.alarms.create('checkljfriends', { "delayInMinutes" : 5 });

        if( !localStorage["ljfriends_username"] ) {
            //console.log( "ljloader.start: username missing" );
            return;
        }

        ljloader.loadFriends();
    },

    /**
     * Wait for two asynchronous requests to finish, then parse both.
     *
     * @returns {undefined}
     */
    loadFriends: function() {
        setTimeout(ljloader.checkRequests.bind(this), 1000);
        var a = new ljrequest( localStorage["ljfriends_username"]  );
        a.createRequest( a.GETFRIENDS, ljloader.parseFriends );
        var b = new ljrequest( localStorage["ljfriends_username"]  );
        b.createRequest( b.FRIENDOF, ljloader.parseFoF );

    },
    parseFriends : function(param, resp) {
        ljloader.friends = ljloader.parseResponse(param, resp);
        ljloader.finished_friends = true;
    },
    parseFoF : function(param, resp) {
        ljloader.fof = ljloader.parseResponse(param, resp);
        ljloader.finished_fof = true;
    },

    /**
     *
     * Waits until both requests finish
     */
    checkRequests: function() {
        if( ljloader.finished_fof && ljloader.finished_friends ) {
            if( ljloader.fof.getResult() === 'OK' && ljloader.friends.getResult() === 'OK' ) {
                ljloader.result = 'OK';
                localStorage.removeItem('ljfriends_error');
                //console.log( "Both requests OK" );
                ljloader.updateStats();
            } else {
                ljloader.result = 'FAIL';
                //console.log( "One of requests error when parsing request" );
                if( "Invalid password" === ljloader.friends.getMessage()) {
                    msg = "Can not log in to Livejournal";
                } else {
                    msg = ljloader.friends.getMessage();
                }
                localStorage['ljfriends_error'] = msg;
                chrome.browserAction.setBadgeText({text: 'Err'});
            }
        } else if( --ljloader.timeout <=0 ) {
            //console.log( 'timeout');
            ljloader.result = 'timeout';
        } else {
            //console.log( 'checking again');
            setTimeout(ljloader.checkRequests.bind(this), 1000);
        }
    },
    getResult: function() {
        return ljloader.result;
    },
            
    /**
     * Parse livejournal response
     * Note that user ID can not be 0 , so we can store lists from 0.
     * @param {string} param
     * @param {string} resp
     * @returns {ljuserlist}
     */
    parseResponse: function (param, resp) {
        if( param=="getfriends" ) param = 'friend';

        var lines = resp.split(/\r?\n/);
        var users = new ljuserlist();
        var re1 = new RegExp( /(?:friend|friendof)_(\d+)_(\w+)$/ );
        var matches, n1=-1, i=0;
        var count = 0;
        var cmd_count = param+"_count"; // friends_count, friendof_count
        var cmd_result = 'success'; // OK | FAIL
        var cmd_errorcode= 'errmsg';
        var txt_error = null;
        var txt_result = null;
        // @todo rewrite this into parser
        while( i<lines.length) {
            if( lines[i] === cmd_errorcode ) {
                if( undefined===lines[i+1] ) {
                    users.setResult( "ERROR" ).setMessage("Incomplete input");
                }
                txt_error = lines[i+1];
                users.setMessage( txt_error );
                i+=2;
                continue;
            }

            if( lines[i] === cmd_result ) {
                if( undefined===lines[i+1]) {
                    users.setResult( "ERROR" ).setMessage("Incomplete input");
                    break;//return users";
                }
                txt_result = lines[i+1];
                users.setResult( txt_result );
                i+=2;
                continue;
            }
            if( lines[i] === cmd_count ) {
                //users.setCount( parseInt( lines[i+1] ) );
                i=i+2;
                continue;
            }

            matches = re1.exec( lines[i] );

            if( null===matches) { i++; continue; }

            n1 = parseInt( matches[1] );
            
            if( n1===0 ) { continue; }

            if( undefined!==lines[i+1] ) { // next line contains value
                users.addProperty( n1, matches[2], lines[i+1] );
            }
            i+=2;
        }
        return users;
    },

    /**
     * Build Mutual friends list
     * @returns {undefined}
     */
    updateStats: function() {
        var coll = new ljcollection();
        coll.setFriends( ljloader.friends );
        coll.setFof( ljloader.fof );
        coll.generateFriends();

        var stats = coll.getStats();

        //
        var t = coll.getStats().getFof();
        t = (t<10000)? t.toString() : '10K+';
        chrome.browserAction.setBadgeText({text: t});

        // Need to send message to a popup if it's open
        //
        var st = new ljstorage();
        var req = st.openDB(
            /**
             * Write stats into DB, restart the timer
             * @param {event} e
             * @returns {undefined}
             */
            function(e) {
                stats.setDay('current');
                var db = e.target.result;
                db.transaction( ['stats'] , "readwrite" ).objectStore("stats").put( stats ).onsuccess = function(e) {
                    //console.log('ljloader.updateStats : written to db');
                    // restart the timer
                };
            }
        );
    }
};
// end of ljfriends.js