// not used
var ljparse_old = {
    _users : {},
    //_stats : new ljstats(),

    // direction can be < or >
    // can be called twice
    addUser: function( name, direction ) {
        var t;
        if( name in ljlist._users ) {
            if( ljlist._users[name]===FRIEND_MINE || ljlist._users[name]===FRIEND_OF ) {
                ljlist._stats.decr( ljlist._users[name] );
                ljlist._stats.incr( FRIEND_MUTUAL );
                ljlist._users[ name ] = FRIEND_MUTUAL;
            }
        } else {
            switch( direction ) {
                case "<" :
                    t = FRIEND_MINE;
                    break;
                case ">" :
                    t = FRIEND_OF;
                    break;
                default:
                    return;
            }
            ljlist._users[ name ] = t;
            ljlist._stats.incr( t );
        }
    },

    //
    parseLine: function(s) {
        if( s.charAt(0)=='#' ) {
            return false;
        } // skip comments

        var r = /^(\<|\>)\s+(\S+)/g;
        var info = r.exec( s );
        if( null == info ) { // console.log('error parsing response '+s);
            return false;
        }
        ljlist.addUser( info[2], info[1] );
    },

    loadData: function( txt ) {
        var s = txt.split(/\r?\n/g);
        s.forEach( ljlist.parseLine );
        for( var u in ljlist._users ) {
        }
    },
    /**
     * @param {ProgressEvent} e The XHR ProgressEvent.
     * @private
     */

    processLJResponse: function(e) {
        var xhr = e.target;
        ljlist.loadData( xhr.responseText );
    },

    getFriendsList: function() {
        var last_date = new Date();
        var xhr = new XMLHttpRequest();
        var username = localStorage["ljfriends_username"];

        xhr.responseType = "text";
        xhr.open("GET", DATA_URL+username, true);
        xhr.setRequestHeader("If-Modified-Since", last_date.toString() );
        xhr.onload = this.processLJResponse.bind(this);
        xhr.send();
    }

};
