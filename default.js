/* 
 * Popup initialization code
 */
document.addEventListener('DOMContentLoaded', function() { 
    ljpopup.refresh();

    // if popup is open, listen to message and refresh the stats.


    chrome.runtime.onConnect.addListener(function(port) {
      console.assert(port.name == "knockknock");
      port.onMessage.addListener(function(msg) {
        if (msg.joke == "Knock knock")
          port.postMessage({question: "Who's there?"});
        else if (msg.answer == "Madame")
          port.postMessage({question: "Madame who?"});
        else if (msg.answer == "Madame... Bovary")
          port.postMessage({question: "I don't get it."});
      });
    });
//    chrome.runtime.onMessage.addListener(
//        function(request, sender, sendResponse) {
//            console.log("Response received by the background refresh");
//            console.log(request);
//            if (request.todo == "refresh_stats") {
//                ljpopup.refresh();
//                sendResponse({farewell: "this is popup. goodbye"});
//            }
//        }
//    );
});


