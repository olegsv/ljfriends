function load_options() {
	var username = localStorage["ljfriends_username"];

	// valid colors are red, blue, green and yellow
	if ( username == undefined ) {
            username = "";
	}
        var elem = document.getElementById("username");
        elem.setAttribute("value",username);

}

/**
 * Save form events
 * @param {type} evt
 * @returns {Boolean}
 */
function save_options(evt) {
  var elem = document.getElementById("username");
  var username = elem.value;

  if( username > "" && username.length>1 && username.match( /^[0-9a-z\_\-]+$/) ) {
      localStorage["ljfriends_username"] = username;
      set_status("Options Saved");

      // send message to re-connect the stats.
      chrome.runtime.sendMessage( { "todo":"load", "username": username}, function(response) {
        console.log("Response received");
      });
      
      setTimeout(function() {
          window.close();
      }, 1750);
  } else {
      set_status("Error saving options", true);
  }
  return false;
}

function set_status( str, is_error ) {
  // Update status to let user know options were saved.
  var status = document.getElementById("status");
  status.innerHTML = str;

}

document.addEventListener('DOMContentLoaded', function() {
  load_options();
  document.querySelector('#save').addEventListener('click', save_options);
  document.querySelector('#username').addEventListener('keyup', function(e) {
      var keyCode = e ? (e.which ? e.which : e.keyCode) : event.keyCode;
      if( keyCode===13 )
          save_options();
  });
  //document.querySelector('#options_form').addEventListener('submit', save_options, false);
});
